window.onload=function(){
    let header=document.querySelector('.header');
    header.innerHTML=`<div class="center box">
        <div class="left">
            <a href="index.html">
                <img src="img/tk_logo.png"/>
            </a> 
        </div>
        <div class="right">
            <a href="set.html" class="set"></a>
            <a href="new.html" class="new"></a>
            <div class="btn logout">
                退出登录
            </div>
        </div>
    </div>`


    // 退出登录
    let logout=document.querySelector('.logout');
    let maskBox=document.querySelector('.maskBox');
    logout.onclick=function(){
        maskBox.style.display="block";
    }
    let yes=document.querySelector('.yes');
    yes.onclick=function(){
        window.location.href="./login.html" 
    }
    let no=document.querySelector('.no');
    no.onclick=function(){
        maskBox.style.display="none";
    }

    // 导航栏
    let navhtml=document.querySelector('.nav');
    navhtml.innerHTML=`<div class="center nav-box">
    <div class="left">
        <p class="p1">Audience Overview</p>
        <p class="p2">Roseville, CA</p>
    </div>
    <div class="right">
        <ul>
            <li>
                <a href="#">首页</a>
            </li>
            <li>
                <a href="#">导航2</a>
            </li>
            <li>
                <a href="#">导航3</a>
            </li>
            <li>
                <a href="#">导航4</a>
            </li>
            <li>
                <a href="#">导航5</a>
            </li>
        </ul>
        <div class="nav-line"></div>
    </div>
</div>`

    var labels = document.querySelector('.nav-box').querySelectorAll('li');
    var line = document.querySelector('.nav-line');
    var initial = 0; 
    var star = 0;
    var time;
    labels.forEach(function(item){
        item.onclick = function(){         
            clearInterval(time);
            animation(item.offsetLeft);
            star = item.offsetLeft;
        }
        item.onmouseover= function(){        
            clearInterval(time);
            animation(item.offsetLeft);
        }
        item.onmouseout= function(){         
            clearInterval(time);
            animation(star);
        }
    })
    // 动画
    function animation(goal){
        initial = line.offsetLeft;
        time = setInterval(function(){
            initial += (goal-initial)/4;
            line.style.left = initial +'px';        
            if(line.offsetLeft==goal){
                clearInterval(time);
            }    
        },30)
    }
    
     //轮播图
    let imglist=document.getElementById('imglist');
    let prev=document.querySelector('.prev');
    let next=document.querySelector('.next');
    let esico=document.getElementById('icoList').getElementsByTagName('li');
    let esicolist=document.querySelector('#icoList');
    let eimglist=document.querySelector('#imglist');
    let imgWidth=600;
    let left=0;
    let timer;
    run();
    function run(){
        if(left<=-imgWidth*3){
            left=0;
        }
        let imgIndex=Math.floor(-left/imgWidth);
        imglist.style.marginLeft=left+'px';
        var n=(left%imgWidth==0)?n=3000:n=10;
        left-=30;
        timer=setTimeout(run,n);
        icochange(imgIndex)
    }

    function imgChange(n){
        let lt=-(n*imgWidth)
        imglist.style.marginLeft=lt+'px';
        left=lt
    }

    prev.onclick=function(){
        let prevgo=Math.floor(-left/imgWidth)-1;
        if(prevgo==-1){
            prevgo=2
        }
        imgChange(prevgo)
        icochange(prevgo)
    }
    next.onclick=function(){
        let nextgo=Math.floor(-left/imgWidth)+1;
        if(nextgo==3){
            nextgo=0
        }
        imgChange(nextgo)
        icochange(nextgo)
    }

    function icochange(m){
        for(let i=0;i<esico.length;i++){
            esico[i].style.backgroundColor=''
            esico[i].style.width='10px'
        }
        if(m<esico.length){
            esico[m].style.backgroundColor='#fff'
            esico[m].style.width='15px'
        }
    }

    esicolist.onclick=function(){
        let tg=event.target;
        let ico=tg.getAttribute("data-id");
        imgChange(ico);
        icochange(ico)
    }

    eimglist.onmouseover=function(){
        clearTimeout(timer)
    }
    eimglist.onmouseout=function(){
        timer=setTimeout(run,3000);
    }

    let geturl="https://edu.telking.com/api/"
    getAjax();
    getAjax('week');

    function getAjax(type='month'){
        var xhr = new XMLHttpRequest();
        xhr.open('GET', geturl+"?type="+type,true);
        xhr.send(null);
        xhr.onreadystatechange = function () {
            if (xhr.status === 200 && xhr.readyState === 4) {
                let res=JSON.parse(xhr.responseText)
                if(res.code==200){
                    if(type=='month'){
                        echart01(res)
                    }else{
                        echart02(res)
                        echart03(res)
                    }
                }
            }
        }
    }

    function echart01(res){
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('line'));

        // 指定图表的配置项和数据
        option = {
            title: {
                text: '曲线图数据展示',
                left: 'center'
            },
            xAxis: {
                type: 'category',
                data: res.data.xAxis
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    data: res.data.series,
                    type: 'line',
                    label: {
                        show: true
                    },
                    areaStyle: {
                        color: '#f3f7fe'
                    },
                    smooth: true
                }
            ]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    }

    function echart02(res){
        let series=res.data.series;
        let xAxis=res.data.xAxis;
        let seriesArr = []
        for(let i=0;i<series.length;i++){
            let seriesData={};
            seriesData.value=series[i];
            seriesData.name=xAxis[i];
            seriesArr.push(seriesData)
        }
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('pie'));

        // 指定图表的配置项和数据
        option = {
            title: {
                text: '饼状图数据展示',
                left: 'center'
            },
            tooltip: {
                trigger: 'item'
            },
            series: [
            {
                type: 'pie',
                radius: '50%',
                data: seriesArr,
                emphasis: {
                    itemStyle: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
            ]
        }

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    }

    function echart03(res){
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('bar'));

        // 指定图表的配置项和数据
        option = {
            title: {
                text: '柱状图数据展示',
                left: 'center'
            },
            xAxis: {
              type: 'category',
              data: res.data.xAxis
            },
            yAxis: {
              type: 'value',
              name: '商品数'
            },
            series: [
              {
                data: res.data.series,
                type: 'bar'
              }
            ]
          };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    }
}

